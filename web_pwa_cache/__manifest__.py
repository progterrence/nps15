# Copyright 2020 Tecnativa - Alexandre D. Díaz
# Copyright 2021 Tecnativa - Pedro M. Baeza
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Web PWA Cache",
    "summary": "Adds support to offline usage and mobile views improvements",
    "version": "13.0.1.0.0",
    "category": "Website",
    "author": "Tecnativa, " "Odoo Community Association (OCA)",
    "website": "https://github.com/OCA/web",
    "license": "AGPL-3",
    "depends": ["web_pwa_oca", "queue_job"],
    "data": [
        "security/security.xml",
        "security/ir.model.access.csv",
        "data/queue_job_function_data.xml",
        # "templates/assets.xml",
        "views/pwa_cache_views.xml",
        "views/res_partner_views.xml",
        "data/data.xml",
    ],
    "qweb": ["static/src/xml/base.xml"],
    "installable": True,
    "auto_install": False,
    'assets': {
        'web.assets_backend': [
            'muk_web_theme/static/src/webclient/**/*.js',
            '/web_pwa_cache/static/src/scss/main.scss',
            '/web_pwa_cache/static/src/js/*.js',
            '/web_pwa_cache/static/src/js/*.scss'
            '/web_pwa_cache/static/src/js/views/*.js',
            '/web_pwa_cache/static/src/js/views/formPWA/*.js',
            '/web_pwa_cache/static/src/js/mixins/*.js',

        ],
    },
}
