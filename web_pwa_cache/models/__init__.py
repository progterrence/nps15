# Copyright 2020 Tecnativa - Alexandre D. Díaz
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
from . import base
from . import ir_ui_view
from . import ir_actions_act_window_view
from . import pwa_cache
from . import ir_translation
