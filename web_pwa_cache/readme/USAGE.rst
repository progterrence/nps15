The 'cache' feature can be enabled per user (by default is disabled for all). To enable it you need add the user to the 'Enable PWA Cache' group.
